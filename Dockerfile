FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY src/*.sln .
COPY src/*.csproj .
COPY src/*.csproj ./aspnetapp/
RUN dotnet restore

# copy everything else and build app
COPY src/. ./aspnetapp/
WORKDIR /app/aspnetapp
ARG SHORT_COMMIT=12345678
ARG TAG_COMMIT=1.0.0
RUN dotnet publish -c Release -o out --version-suffix "RA.${SHORT_COMMIT}" -p:VersionPrefix="${TAG_COMMIT}"

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
COPY --from=build /app/aspnetapp/out ./
ENTRYPOINT ["dotnet", "aspnetapp.dll"]