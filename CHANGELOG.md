# CHANGELOG

<!--- next entry here -->

## 1.0.1
2020-02-12

### Fixes

- new format tag (0ce7c133df51a2f2e68f6cee63ca729a602bd860)

## 1.0.0
2020-02-04

### Breaking changes

#### BREAKING CHANGE: Adicionar README.md (b1ddd5da13fcee7bf2a2cf6deabc46b9da4e922d)

Adicionar README.md

## 0.5.0
2020-02-04

### Breaking changes

#### BREAKING CHANGE: Adicionar LICENSE. (1c11c548ff37698c4e7d7f13f73a43f613932f77)

Adicionar LICENSE.

## 0.4.0
2020-02-04

### Breaking changes

#### BREAKING CHANGE: The graphiteWidth option has been removed. (293cc21275ed700510f6eab7532864f9358c2019)

The graphiteWidth option has been removed.

## 0.3.0
2020-01-29

### Features

- **ci:** new ci (5707fa2b66a45d142f4f0c7e5526ae1c194cba5d)

## 0.2.4
2020-01-27

### Fixes

- **cut var:** remove v from tag version (77fcd66a2a42c35ad5afa233ea4f1171d1fd9bad)

## 0.2.3
2020-01-27

### Fixes

- **change version:** remove v from version tag (b023c0d06f958edd2ec67b9ccd07ba277aa78f83)

## 0.2.2
2020-01-27

### Fixes

- **remove dot:** remove dot on params build kaniko (1991dd2831cc785f019c87a9b4effbdb800e22b8)

## 0.2.1
2020-01-27

### Fixes

- **dockerfile:** change args (efdc9c4fe2c6afb6db19ad9ff470615b48c6c820)

## 0.2.0
2020-01-27

### Features

- **args:** dockerfile pass (8037729a8c6d1432d3b37cdbf0dbde932eb91b34)

## 0.1.38
2020-01-24

### Fixes

- **dockerfile:** new command test123 (9d2ec296d0c9b042492d3e4edf53e407bc46d934)

## 0.1.37
2020-01-24

### Fixes

- **dockerfile:** new command test5 (b8b570a730c5f07073bc466ccd47a326e4a83d8c)

## 0.1.36
2020-01-24

### Fixes

- **dockerfile:** new command test5 (2bd9d34f28c280e095229ca688258edf0e23990c)

## 0.1.35
2020-01-24

### Fixes

- **dockerfile:** new command test2 (2dedf7360b72d7dd9ef0bc6547da062306dcc843)

## 0.1.34
2020-01-24

### Fixes

- **dockerfile:** new command test (51f8b8c5340452f556b51badb192448c29be9175)

## 0.1.33
2020-01-24

### Fixes

- **12:** nntt (c8f26704185de3cac8dec68e8745b08423d4995b)

## 0.1.32
2020-01-24

### Fixes

- **nn:** n^23 (e2d8e117cad638bf886f6c0efc2a949b9104b92d)

## 0.1.31
2020-01-24

### Fixes

- **ci:** new test32 (ea7224097fd657a7bfdb6812ddfccc07904b4b50)

## 0.1.30
2020-01-24

### Fixes

- **ci:** new test31 (7c9967976735e4c8d335df2053954537606f0491)

## 0.1.29
2020-01-24

### Fixes

- **ci:** new test30 (ce7a18226cc698986c8f3cf84824f5e8205093f4)

## 0.1.28
2020-01-24

### Fixes

- **ci:** new test22 (2310a1f6a688957e9a668e50ca9ff86e79e489dc)

## 0.1.27
2020-01-24

### Fixes

- **ci:** new test21 (a2f12b0fbb8a3bee30f0f6a704c747c2153c66bf)

## 0.1.26
2020-01-24

### Fixes

- **ci:** new test20 (596510b3883b8dc78713b72b5809157045fa9592)

## 0.1.25
2020-01-24

### Fixes

- **ci:** new test18 (388af26419b09cea40544094875d40601ae9bc62)

## 0.1.24
2020-01-24

### Fixes

- **ci:** new test15 (ab728a82787e354b6132c04a49298549b0cfced5)
- **ci:** new test17 (a1f52a2ff8d90472d5a3d0394cc730838de99007)

## 0.1.23
2020-01-24

### Fixes

- **ci:** new test15 (ee4adb84cd83dcdcf6a4d0f919b9bfd35992c949)

## 0.1.22
2020-01-24

### Fixes

- **ci:** new test14 (9475e25c286f274b595027400d211df346b27fb1)

## 0.1.21
2020-01-24

### Fixes

- **ci:** new test13 (f3d34fd68eec080ed38537595747b64f60b815cc)

## 0.1.20
2020-01-24

### Fixes

- **ci:** new test12 (e2a7233e8e9549c88c00d21e6b08cbc76755dc24)

## 0.1.19
2020-01-24

### Fixes

- **ci:** new test11 (e8f7a9e904a33d25e125d369848fb24ee89afed1)

## 0.1.18
2020-01-24

### Fixes

- **ci:** new test10 (9209277a1fbfee2cc45e5bd1569614c60dc77f1a)

## 0.1.17
2020-01-24

### Fixes

- **ci:** new test9 (0971b114c0e39d15cc2c341ba3330b55ee5a9555)

## 0.1.16
2020-01-24

### Fixes

- **ci:** new test8 (67e3dfc581219266761c0029726780d92bdbd326)

## 0.1.15
2020-01-24

### Fixes

- **ci:** new test7 (f05f0d6ab6e66221c4c12fe4b658935b2703c916)

## 0.1.14
2020-01-24

### Fixes

- **ci:** new test6 (f9d5551f904698c9b5f3b8c3b5500ebebde980ea)

## 0.1.13
2020-01-24

### Fixes

- **ci:** new test5 (ebfecfaf4bab1c5d7539c2591b7f09dad51263bc)

## 0.1.12
2020-01-24

### Fixes

- **ci:** new test4 (8c62c60321139fac7248447d6c387f8d6466024f)

## 0.1.11
2020-01-24

### Fixes

- **ci:** new test3 (4d4ffdffa9286ea3c0756620d99164674711c2d9)

## 0.1.10
2020-01-24

### Fixes

- **ci:** new test2 (a5db638045fa65d1982ecf699aaae7213e746281)

## 0.1.9
2020-01-24

### Fixes

- **ci:** new test (4005db550af7166f72642c8ffe14f716d1c04612)

## 0.1.8
2020-01-24

### Fixes

- **ci:** test with $ vars (f70ea4ae3c8e6e94332990666f6d82ec306bb225)

## 0.1.7
2020-01-24

### Fixes

- **dockerfile:** test new vars params (32733a52d9098fc5b5f5fbb4416686d1ffa51347)

## 0.1.6
2020-01-24

### Fixes

- **dockerfile:** set ARGs from gitlab ci (6e572dcea6e481f29dc7489bb261c6d2fe052298)

## 0.1.5
2020-01-24

### Fixes

- **dockerfile:** change vars envs (398a94b1d0d91199c1c435715f51582c7ce5fbe9)

## 0.1.4
2020-01-24

### Fixes

- **dockerfile:** change vars (b1ec660db1589bd3254f30868f03d8e3f9201713)

## 0.1.3
2020-01-24

### Fixes

- **dockerfile:** change dockerfile release (d394cac1caac3b5d28c88c5923b3a39653d983ff)

## 0.1.2
2020-01-24

### Fixes

- **solution:** remove sln file (53d01c04c25e265d81e458f69c00ce7906e6b4f8)

## 0.1.1
2020-01-24

### Fixes

- **remove:** remove copy sln (eb13090c1cb634758b7807060a4c906c0b79ede5)
- **version:** set version on build image docker (f2ff2bd340087b1e9de23f61f26b07900524feaf)
- **new path:** set version on build dotnet (7f3f4d6abc6c2e161874f67def5695c1a5e9e9ed)

## 0.1.0
2020-01-23

### Features

- **adding:** add git ignore (db79bc4dcb9bd77ad34ad3ef8cef4ee0e1f9de36)